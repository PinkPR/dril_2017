KDIR = /lib/modules/$(shell uname -r)/build

obj-m := ventilo.o

SRC = ventilo.c

all: ventilo.ko

ventilo.ko: $(SRC)
	$(MAKE) -C $(KDIR) SUBDIRS=$(PWD) modules

ins: ventilo.ko
	sudo insmod ventilo.ko

.PHONY: ins

rm:
	sudo rmmod ventilo.ko

.PHONY: rm

test:
	sudo python test.py

.PHONY: test

clean:
	rm -f ventilo.ko  ventilo.mod.c  ventilo.mod.o  ventilo.o modules.order  Module.symvers
